package cz.cvut.k36.omo.hw.hw01;

public class Homework1 {

    private int hCallsCounter = 0;
    private int iCallsCounter = 0;

    boolean f() {
        iCallsCounter++;
        return true;
    }

    boolean g() {
        iCallsCounter++;
        return false;
    }

    int h() {
        iCallsCounter++;
        hCallsCounter++;
        return hCallsCounter;
    }

    int i() {
        iCallsCounter++;
        return iCallsCounter;
    }

}
